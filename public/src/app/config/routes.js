'user strict';
angular.module('StarterApp')

  //we're gonna give angular ui a spin
	.config(function ($stateProvider, $urlRouterProvider, $locationProvider){

		  // For any unmatched url, redirect to /login
  		$urlRouterProvider.otherwise('/');

  		$stateProvider
  			
        .state('home', {
          url: '/index.php/app',
          templateUrl: "public/views/login.html",
          controller: 'LoginController',
          data: {
            requireLogin: false
          }
        })

  			.state('login', {
  				url: '/',
  				templateUrl: "public/views/login.html",
  				controller: 'LoginController',
          data: {
            requireLogin: false
          }
  			})

  			.state('forgot-password', {
  				url: "/index.php/app/forgot-password",
  				templateUrl: "public/views/forgot-password.html",
  				controller: 'ForgotPasswordController',
          data: {
            requireLogin: false
          }
  			})

        .state('app', {
          abstract: true,
          url: '/index.php/app/application',
          templateUrl: "public/views/application.html",
          controller: 'AppController',
          data: {
            requireLogin: true,
          }
        })

          .state('app.dashboard', {
            url: '/dashboard',
            controller: 'AppController',
            data: {
              requireLogin: false,
            }  
          });

		$locationProvider.html5Mode( true );

	});