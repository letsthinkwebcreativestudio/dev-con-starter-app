'use strict';
angular.module('StarterApp')

	.service('loginModal', function ($modal, $rootScope){

		function assignCurrentUser (user){
			$rootScope.assignCurrentUser = user;
			return user;
		}

		return function(){
			var instance = $modal.open({
				templateUrl: 'public/views/partials/modal/login.html',
				controller: 'LoginModalController',
      			controllerAs: 'LoginModalController'
			});
			return instance.result.then(assignCurrentUser);
		};

	});