'use strict';
angular.module('StarterApp')
	
	.run(function ($rootScope){

		$rootScope.$on('$stateChangeStart',	function (event, toState, toParams){
			var requireLogin = toState.data.requiredLogin;

			if (requireLogin && typeof $rootScope.currentUser === 'undefined') {
      			event.preventDefault();
      			// get me a login modal!
      			loginModal()

			        .then(function () {
			          
			          return $state.go(toState.name, toParams);
			        
			        })

			        .catch(function () {
			        	
			        	return $state.go('welcome');

			        });
    		}
		});
	});