'use strict';
angular.module('StarterApp')
	.controller('ForgotPasswordController', ['$scope', '$location', 
		function ($scope, $location){
			
			$scope.title = 'Password Recovery';
						
		}
	]).controller('LoginController', ['$scope', '$location', 
		function ($scope, $location){
			
			$scope.title = 'StarterApp Login';
			
						
		}
	]).controller('AppController', ['$scope', '$location', 
		function ($scope, $location){
			
			$scope.title = 'StarterApp Login';
						
		}
	]);