'use strict';

angular.module('StarterApp')
.controller('LoginModalController', function ($scope, UsersApi) {

  this.cancel = $scope.$dismiss;

  this.submit = function (email, password) {

    UsersApi.login(email, password).then(function (user) {
      $scope.$close(user);
    });
    
  };

});