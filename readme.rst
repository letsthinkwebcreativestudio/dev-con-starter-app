###################
DevCup Starter App
###################

A bare Codeigniter 3 & AngularJS starter app kit, that has login and basic CRUD capabilities
made by team LetsThinkWeb

*******************
Release Information
*******************

This repo contains in-development code for future releases. 



**************************
Changelog and New Features
**************************

No releases yet


*******************
Requirements
*******************

* Apache 2.1
* MySQL 5.0 or higher
* PHP version 5.4 or newer is recommended.
* Bower [Install Bower](http://bower.io/)
* Composer [Install Composer](https://getcomposer.org/)


For Windows users:
You can Either install [wamp](http://www.wampserver.com/en/) or [xampp](https://www.apachefriends.org/index.html)

************
Installation
************
clone the repo in your localhost

For Windows
you can either use [gitbash ](https://msysgit.github.io/)or [powershell](http://powershell.com/cs/)

```
#!git

$ cd /c/wamp/www/

/* We are using the directory name devcup for development pre requisites purposes */

$ mkdir devcup 

$ cd devcup

/* The clone url can be obtain on the right upper most corner of bitbucket */

$ git clone git@bitbucket.org:yourbitbucketusername/dev-con-starter-app.git


$ bower install

```






***************
Acknowledgement
***************

Starter App Kit, is an official tool for Team LetsThinkWeb