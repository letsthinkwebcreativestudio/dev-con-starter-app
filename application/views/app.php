<!DOCTYPE html>
<html lang="en" ng-app="StarterApp">
    <head>
        <title>LetsThinkWeb : DevCup Starter App</title>
        <base href='/devcup/'>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/lib/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/themes/flatly/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/styles.css" />
    </head>
 	<body>
    	<div class="container-fluid">
            <div class="page-header">
                <h1 class="weight-bold hidden"> Starter Application </h1>
            </div>

            <div class="row">
                <div class="col-sm-12" ui-view>

                </div>
            </div>
        </div>
        <script src="//use.typekit.net/kki2ujq.js"></script>
        <script>try{Typekit.load();}catch(e){}</script>
        <script src="<?php echo base_url(); ?>public/lib/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>public/lib/angular/angular.min.js"></script>
        <script src="<?php echo base_url(); ?>public/lib/angular-route/angular-route.min.js"></script>
        <script src="<?php echo base_url(); ?>public/lib/angular-ui-router/release/angular-ui-router.min.js"></script>
        <script src="<?php echo base_url(); ?>public/lib/angular-resource/angular-resource.min.js"></script>
        <script src="<?php echo base_url(); ?>public/src/app/app.js"></script>
        <script src="<?php echo base_url(); ?>public/src/app/services/modal.js"></script>
        <script src="<?php echo base_url(); ?>public/src/app/config/interceptor.js"></script>
        <script src="<?php echo base_url(); ?>public/src/app/config/routes.js"></script>
        <script src="<?php echo base_url(); ?>public/src/app/controllers/app-controller.js"></script>
        <script src="<?php echo base_url(); ?>public/src/app/app.init.js"></script>
    </body>
    <footer>
    		
    </footer>
</html>